# DRIM Projets Soapui

## Simulateur_Client_WADO_RS

Préalablement à l'import du projet DRIM-WADO-RS-soapui-project.xml sur GWT, plusieurs instances doivent être créées. 
Chacune de ces instances doit faire apparaître un nombre limité de test suite "actives". 
Pour cela, il est nécessaire de désactiver certaines test suite afin de correspondre au mapping décrit ci-dessous :

### Simulateur_Client_WADO_RS_Nominal

Instance "Simulateur_Client_WADO_RS_Nominal", instances A ACTIVER :
* GetSerie
* GetStudy
* GetInstance 
* GetFrame
* DRIM-M_SO_NATIF_PERFORMANCE
* DRIM-M_SO_PROXY_PERFORMANCE
* DRIM-M_SO_REPONSE_WADO.01
* DRIM-M_SO_REPONSE_WADO.02
* DRIM-M_SO_REPONSE_WADO.03 
* DRIM-M_SO_REPONSE_WADO.04
* DRIM-M_SO_REPONSE_WADO.06
* DRIM-M_SO_CONVERSION 
* DRIM-M_SO_mTLS.01

#### Génération du JKS associé

Pour générer le JKS à partir du fichier P12 envoyé par l'ANS, utilisez la commande suivante (en utilisant toujours le mot de passe de la clé privée, également fourni par l'ANS) :

```
keytool -importkeystore -srckeystore asip-p12-EL-TEST-SERV-SSL_SERV-db5.p12 -srcstoretype pkcs12 -destkeystore asip-p12-EL-TEST-SERV-SSL_SERV-db5.jks -deststoretype jks
```

### Simulateur_Client_WADO_RS_NOK_Liste_Blanche

Instance "Simulateur_Client_WADO_RS_NOK_Liste_Blanche", instances A ACTIVER :
* GetSerie
* GetStudy
* GetInstance 
* GetFrame
* DRIM-M_SO_REPONSE_WADO.07

#### Génération du JKS associé

Pour générer le JKS à partir du fichier P12 envoyé par l'ANS, utilisez la commande suivante (en utilisant toujours le mot de passe de la clé privée, également fourni par l'ANS) :

```
keytool -importkeystore -srckeystore asip-p12-EL-TEST-SERV-SSL_SERV-db9.p12 -srcstoretype pkcs12 -destkeystore asip-p12-EL-TEST-SERV-SSL_SERV-db9.jks -deststoretype jks
```

### Simulateur_Client_WADO_RS_Certificat_Revoque

Instance "Simulateur_Client_WADO_RS_Certificat_Revoque", instances A ACTIVER :
* GetSerie
* GetStudy
* GetInstance 
* GetFrame
* DRIM-M_SO_mTLS.03

#### Génération du JKS associé

Pour générer le JKS à partir du fichier P12 envoyé par l'ANS, utilisez la commande suivante (en utilisant toujours le mot de passe de la clé privée, également fourni par l'ANS) :

```
keytool -importkeystore -srckeystore asip-p12-EL-TEST-SERV-SSL_SERV-db7.p12 -srcstoretype pkcs12 -destkeystore asip-p12-EL-TEST-SERV-SSL_SERV-db7.jks -deststoretype jks
```

### Simulateur_Client_WADO_RS_Certificat_Du_Marche

Instance "Simulateur_Client_WADO_RS_Certificat_Du_Marche", instances A ACTIVER :
* GetSerie
* GetStudy
* GetInstance 
* GetFrame
* DRIM-M_SO_mTLS.02


#### Génération du JKS associé

Se connecter à gazelle-ans.kereval.com VM via SSH

```
sudo su
cd /etc/letsencrypt/live/db2.test.mesimagesmedicales.fr/
```

Créez une copie de la clé avec un mot de passe :

```
openssl rsa -aes256 -in privkey.pem -out privkey-withpass.pem
```

Créez le fichier P12 puis le fichier JKS. Réutilisez le mot de passe de la clé privée précédemment définie dans l'étape précédente :

```
openssl pkcs12 -export -in fullchain.pem -inkey privkey-withpass.pem -out db2.test.mesimagesmedicales.fr.p12
keytool -importkeystore -destkeystore db2.test.mesimagesmedicales.fr.jks -srckeystore db2.test.mesimagesmedicales.fr.p12 -srcstoretype PKCS12
```

Importez le JKS dans Gazelle Webservice Tester et liez-le au projet.
